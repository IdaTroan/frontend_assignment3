# Assignment 3: Pokèmon Trainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.0.

In this assignment we were to build a Pokèmon Trainer web application using Angular. A trainer is allowed to collect Pokèmon from the 1st generation, which amounts to 151 different pokèmon.

In this assignment we were to build a Pokèmon Trainer web application using Angular. A trainer will have the ability to first of all log in, and then collect any of the 151 first generation pokèmon available. Once a pokèmon is collected, it will be added to their collection stored on their user in the API. Further the trainer will be able to check their profile, where all their pokèmon will be listed. Here they can release their pokèmon if they so wish.

## Component tree

A component tree was designed before starting to develop the project. The component tree represents our plan for working through the assignment, and it is a fairly accurate model of the finished product.

The component tree can be found [here](./Assignment_3_-_Component_Tree.pdf).

## Install

This project makes use of node and npm, so to install the application type the following command in the terminal:

```shell
npm i
```

This will install all the dependencies necessary to run the application locally.

## Usage

To run the application type the following command in the terminal:

```shell
ng serve -o
```

The application will be hosted locally at `http://localhost:4200/`. Alternatively the application is also live, and can be tested at [https://assignment-pokemontrainer.netlify.app/](https://assignment-pokemontrainer.netlify.app/)

The API used in this application is hosted at [https://lost-in-translation-api-production.up.railway.app/](https://lost-in-translation-api-production.up.railway.app/)

## Contributing

All code have been written by
[Ida Trøan](https://gitlab.com/IdaTroan)
& [Kim-Reino Hjelde](https://gitlab.com/Kimreinh)
