/**
 * To avoid magic strings we are creating constant variables for use
 * when interacting with the sessionStorage. These are the keys/names
 * of the sessionStorage entries.
 */
export enum StorageKeys {
    Trainer = 'trainer-user',
    Pokemon = 'pokemon'
}