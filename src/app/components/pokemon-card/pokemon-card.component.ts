import { Component, EventEmitter, Input, Output } from '@angular/core';
import { regExpEscape } from '@ng-bootstrap/ng-bootstrap/util/util';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css'],
})
export class PokemonCardComponent {
  /**
   * Implementing trainer service, to get access to the trainer state.
   * @param trainerService Trainer state and functionality.
   */
  constructor(private readonly trainerService: TrainerService) {}

  /**
   * Instantiate variables passed to the pokemon-card from parent components.
   * To be able to use the passed down values when displaying the card.
   */
  @Input() name!: string;
  @Input() url!: string;
  @Input() buttonType!: string;
  @Input() buttonText!: string;
  @Input() component!: string;

  /**
   * Instantiate variables passed from pokemon-card to the parent components.
   */
  @Output() sendId = new EventEmitter<{ name: string }>();

  /**
   * Variables to be used in the card, values are assigned OnInit.
   */
  id!: any;
  collected!: boolean;

  /**
   * When component is rendered we take the url sent from the parent component,
   * regex to only get the id and send this to the component to use for displaying the pokemon sprites.
   * Also go through the array of pokemon belonging to the trainer to check whether the pokemon
   * should be displayed as collected.
   */
  ngOnInit() {
    let regex = /(\/\d+)/;
    let match = this.url.match(regex);
    this.id = match![0];

    if (this.trainerService.trainer?.pokemon.includes(this.name)) {
      this.collected = true;
    }
  }

  /**
   * When button is clicked, send the pokemon name to the parent component.
   */
  onSendId() {
    this.sendId.emit({ name: this.name });
  }
}
