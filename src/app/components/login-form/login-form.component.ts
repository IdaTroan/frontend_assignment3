import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent {
  @Output() login: EventEmitter<void> = new EventEmitter();

  loading: boolean = false;

  /**
   * Injecting services for contextual state
   */
  constructor(
    private readonly loginService: LoginService,
    private readonly trainerService: TrainerService
  ) {}

  /**
   * Performs username input validation.
   * If valid, calls injected login service function to handle login logic.
   * service function will return an Observable<Trainer> that is subscribed to here.
   * Returned trainer is set as trainer in state.
   * login is passed along to parent component.
   * @param loginForm
   */
  public loginSubmit(loginForm: NgForm): void {
    if (loginForm.valid) {
      this.loading = true;
      const { username } = loginForm.value;

      this.loginService.login(username).subscribe({
        next: (user: Trainer) => {
          this.trainerService.trainer = user;
          this.login.emit();

          this.loading = false;
        },
        error: (error) => {
          console.error(error.message);
        },
      });
    } else {
      console.log('debug msg: loginform input invalid');
    }
  }
}
