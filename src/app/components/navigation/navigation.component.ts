import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from 'src/app/const/storage-keys.const';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from 'src/app/utils/storage/storage.util';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent {
  /**
   * Implement trainer service to get access to trainer state, and router
   * to get access to router methods.
   * @param trainerService Trainer state and functionality.
   * @param router Router state and functionality.
   */
  constructor(
    private trainerService: TrainerService,
    private readonly router: Router
  ) {}

  /**
   * Get trainer state
   */
  public get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
  }

  /**
   * Variable used with the collapse button for the navigation bar, when navbar is longer than the screen width.
   */
  isNavBarCollapsed: boolean = true;

  /**
   * When user clicks log out button, send a "are you sure" confirmation. If user presses ok we clear the user from storage,
   * update trainer state and navigate user back to login.
   */
  handleLogout(): void {
    if (window.confirm('Are you sure you want to log out?')) {
      StorageUtil.storageClear(StorageKeys.Trainer);
      this.trainerService.trainer = StorageUtil.storageRead(
        StorageKeys.Trainer
      );
      this.router.navigate(['login']);
    }
  }
}
