import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer-card',
  templateUrl: './trainer-card.component.html',
  styleUrls: ['./trainer-card.component.css'],
})
export class TrainerCardComponent {
  /**
   * Injecting services for access to contextual state
   */
  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonService: PokemonService
  ) {}

  /**
   * Flag for conditional alert display
   */
  released: boolean = false;

  /**
   * get trainer from service
   */
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
  }

  /**
   * get number of pokemon caught from service
   */
  get pokemonCounter(): Number | undefined {
    return this.trainerService.trainer?.pokemon.length;
  }

  /**
   * handles click on dismissable alert
   */
  dismiss(): void {
    this.released = false;
  }

  /**
   * Calls removeAllPokemon() from injected service and uses reponse to update trainer in context.
   * User gets confirmation prompt that must be accepted, and a dismissable alert notification after.
   * see: released and dismiss()
   */
  releaseAll(): void {
    if (
      window.confirm(
        'This will release all your collected Pokèmon. Are you sure?'
      )
    ) {
      this.pokemonService.removeAllPokemon().subscribe({
        next: (response: any) => {
          this.trainerService.trainer = response;
        },
        error: (error: HttpErrorResponse) => {
          console.log('Error', error.message);
        },
      });
      this.released = true;
    }
  }
}
