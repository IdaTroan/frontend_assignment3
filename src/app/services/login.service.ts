import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { Trainer } from '../models/trainer.model';

const { apiTrainers, apiKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private readonly http: HttpClient) {}

  /**
   * Driver function for login logic.
   * check username with helper function checkUsername(username).
   * conditional logic returns existing trainer or creates new with helper function createTrainer(username).
   * @param username string representation of a trainer username
   * @returns Trainer object, see src\app\models\trainer.model.ts
   */
  public login(username: string): Observable<Trainer> {
    return this.checkUsername(username).pipe(
      switchMap((trainer: Trainer | undefined) => {
        if (trainer === undefined) {
          return this.createTrainer(username);
        }
        return of(trainer);
      })
    );
  }

  /**
   * Hidden helper function that makes http get request to look for trainer's username.
   * returns response, Trainer object if exists or undefined if not exists.
   * @param username string representation of a trainer username
   * @returns Trainer object, see src\app\models\trainer.model.ts OR undefined as type of Observable
   */
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http
      .get<Trainer[]>(`${apiTrainers}?username=${username}`)
      .pipe(map((response: Trainer[]) => response.pop()));
  }

  private createTrainer(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: [],
    };

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey,
    });

    return this.http.post<Trainer>(apiTrainers, trainer, {
      headers,
    });
  }
}
