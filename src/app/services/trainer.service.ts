import { Injectable } from '@angular/core';
import { StorageKeys } from '../const/storage-keys.const';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage/storage.util';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _trainer?: Trainer;

  /**
   * Getter for trainer in context
   */
  public get trainer(): Trainer | undefined {
    return this._trainer;
  }

  /**
   * Setter for trainer in context. Uses StorageUtil to save to browser storage.
   */
  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }
}
