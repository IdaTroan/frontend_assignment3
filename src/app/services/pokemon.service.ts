import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { StorageKeys } from '../const/storage-keys.const';
import { Pokemon, RootObject } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage/storage.util';
import { TrainerService } from './trainer.service';

const { apiPokemon, apiKey, apiTrainers } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private _pokemon: Pokemon[] = [];
  private _error: string = '';

  /**
   * Get pokemon, if storage contains pokemon read and populate pokemon state with pokemon from sessionStorage.
   * (Originally meant to call findAllPokemon() here for the API request but because of pagination it made 6 calls compared to our now 1.)
   */
  get pokemon(): Pokemon[] {
    if (!StorageUtil.storageRead(StorageKeys.Pokemon)) {
      return this._pokemon;
    } else {
      return StorageUtil.storageRead(StorageKeys.Pokemon)!;
    }
  }

  get error(): string {
    return this._error;
  }

  /**
   * Implement HttpClient to make Http requests to the api, and trainer service for trainer state and functionality.
   * @param http Http state and functionality.
   * @param trainerService Trainer state and functionality.
   */
  constructor(
    private readonly http: HttpClient,
    private readonly trainerService: TrainerService
  ) {}

  /**
   * Make a API Get request to the api, and assign the result to sessionStorage and pokemon state.
   * If not successful return a error.
   */
  public findAllPokemon(): void {
    this.http.get<RootObject>(apiPokemon).subscribe({
      next: (response) => {
        this._pokemon = StorageUtil.storageRead(StorageKeys.Pokemon)!;
        StorageUtil.storageSave<Pokemon[]>(
          StorageKeys.Pokemon,
          response.results!
        );
      },
      error: (error) => {
        this._error = error.message;
      },
    });
  }

  /**
   * Check if there is a trainer, and if the pokemon is already in the trainer's collection.
   * If not make a Patch API request, adding the pokemon to the existing array of pokemon.
   * @param pokemon Pokemon to be added to the trainer's collection.
   * @returns The patched trainer.
   */
  addPokemon(pokemon: string): Observable<any> {
    if (!this.trainerService.trainer) {
      throw new Error('addPokemon: There is no trainer.');
    }

    const trainer: Trainer = this.trainerService.trainer;

    if (trainer.pokemon.includes(pokemon)) {
      throw new Error('addPokemon: Pokemon already in collection.');
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });

    console.log(trainer.id);
    return this.http.patch<Trainer>(
      `${apiTrainers}/${trainer.id}`,
      {
        pokemon: [...trainer.pokemon, pokemon],
      },
      {
        headers,
      }
    );
  }

  /**
   * Check if there is a trainer, and confirm that the pokemon is in trainer's collection.
   * If it is, send patch request with pokemon array where pokemon passed as argument is deleted
   * from collection with an array splice operation.
   * @param pokemon Pokemon to be removed from the trainer's collection.
   * @returns The patched trainer.
   */
  removePokemon(pokemon: string): Observable<any> {
    if (!this.trainerService.trainer) {
      throw new Error('addPokemon: There is no trainer.');
    }

    const trainer: Trainer = this.trainerService.trainer;

    if (!trainer.pokemon.includes(pokemon)) {
      throw new Error('removePokemon: Pokemon not in collection.');
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });

    const index = this.trainerService.trainer.pokemon.indexOf(pokemon);
    if (index > -1) {
      this.trainerService.trainer.pokemon.splice(index, 1);
    }

    console.log(trainer.id);
    return this.http.patch<Trainer>(
      `${apiTrainers}/${trainer.id}`,
      {
        pokemon: this.trainerService.trainer.pokemon,
      },
      {
        headers,
      }
    );
  }

  /**
   * Verify that trainer exists.
   * If it is, send patch request with empty pokemon array.
   * @returns The patched trainer.
   */
  removeAllPokemon(): Observable<any> {
    if (!this.trainerService.trainer) {
      throw new Error('addPokemon: There is no trainer.');
    }

    const trainer: Trainer = this.trainerService.trainer;

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });

    console.log(trainer.id);
    return this.http.patch<Trainer>(
      `${apiTrainers}/${trainer.id}`,
      {
        pokemon: [],
      },
      {
        headers,
      }
    );
  }
}
