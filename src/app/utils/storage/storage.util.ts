export class StorageUtil {

  /**
   * Save the given value to storage, with the given key.
   * @param key Name of storage
   * @param value Value to be stored in storage
   */
  public static storageSave<T>(key: string, value: T) : void {
    sessionStorage.setItem(key, JSON.stringify(value))
  }

  /**
   * Read the current value of the storage, based on the given key.
   * @param key Name of storage
   * @returns The stored value in storage, parsed into a JSON object.
   */
  public static storageRead<T>(key: string) : T | undefined {
    const storedValue = sessionStorage.getItem(key);

    try {
      if (storedValue) {
        return JSON.parse(storedValue) as T;
      }
      return undefined;
    } catch (e) {
      sessionStorage.removeItem(key);
      return undefined;
    }
  }

  /**
   * Delete the entire storage entry based on the given key.
   * @param key Name of storage
   */
  public static storageClear(key: string) {
    sessionStorage.removeItem(key)
  }
}