import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage {
  /**
   * setup access to router functionality
   */
  constructor(private readonly router: Router) {}

  /**
   * if child component sends a successful login, result of that login is handled here.
   * navigates user to main page, the catalogue.
   */
  handleLogin(): void {
    this.router.navigateByUrl('/pokemon-catalogue');
  }
}
