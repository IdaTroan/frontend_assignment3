import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { StorageKeys } from 'src/app/const/storage-keys.const';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from '../../utils/storage/storage.util';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css'],
})
export class PokemonCataloguePage {
  /**
   * Implement the pokemon and trainer service to get access to their state and functionality.
   * @param pokemonService Pokemon state and functionality.
   * @param trainerService Trainer state and functionality.
   */
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly trainerService: TrainerService
  ) {}

  /**
   * Variable used in pagination, by default set to 1 so it will start from the beginning.
   */
  p: number = 1;

  /**
   * Get pokemon state.
   */
  get pokemon(): Pokemon[] {
    return this.pokemonService.pokemon
  }

  /**
   * Make the API request to get all the pokemon, if they are not already stored in session storage.
   * The only Pokemon API request made.
   */
  ngOnInit(): void {
    if (!StorageUtil.storageRead<Pokemon[]>(StorageKeys.Pokemon)) {
      this.pokemonService.findAllPokemon();
    }
  }

  /**
   * When button is clicked call addPokemon() to add the pokemon to the trainer collection.
   * Then update the trainer with the updated trainer response.
   * If not successful return an error.
   * @param data Name of the pokemon we want added to the collection array, sent from child component.
   */
  addToCollection = (data: { name: string }) => {
    this.pokemonService.addPokemon(data.name).subscribe({
      next: (response: any) => {
        this.trainerService.trainer = response;
      },
      error: (error: HttpErrorResponse) => {
        console.log('Error', error.message);
      },
    });
  };
}
