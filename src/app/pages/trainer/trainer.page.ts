import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage {
  /**
   * Injecting services for context
   */
  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonService: PokemonService
  ) {}

  /**
   * Uses injected service to populate an instance of an array with pokemon that exists in trainer, aka they are collected.
   * Uses array of all available pokemon matching with trainer's pokemon on name. see: get pokemon()
   */
  get collected(): any[] {
    let ret: any[] = [];
    this.pokemon.forEach((element) => {
      if (this.trainerService.trainer?.pokemon.includes(element.name)) {
        ret.push(element);
      }
    });
    return ret;
  }

  /**
   * Uses injected service to retrieve array with all available pokemon.
   * see: get collected()
   */
  get pokemon(): Pokemon[] {
    return this.pokemonService.pokemon;
  }

  /**
   * Calls function in injected pokemon service and uses response to update trainer in context.
   * removes a single pokemon from trainer's collection based on name.
   * @param data
   */
  removeFromCollection = (data: { name: string }) => {
    this.pokemonService.removePokemon(data.name).subscribe({
      next: (response: any) => {
        this.trainerService.trainer = response;
      },
      error: (error: HttpErrorResponse) => {
        console.log('Error', error.message);
      },
    });
  };
}
